C++-written mini game with no GUI that has the player to try to find the hidden treasure before the enemy does.
If the player steps on a hidden trap on the way, he prays to the RNG gods that he will be able to disarm the trap, or he dies.
Random amount of enemies are spawned at the beginning of the game to chase the player until he wins or they kill him.
The player is able to collect knives on his way, with which he will fight off the enemy AI, once again with the help of the RNG gods.
If the player has no knives - he dies.
